# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FieldDataDialog
                                 A QGIS plugin
 Topaze
                             -------------------
        begin                : 2022-09-29
        git sha              : $Format:%H$
        copyright            : (C) 2022 by Jean-Mari ARSAC
        email                : jmarsac@arsac.wf
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt5 import QtCore, QtWidgets, uic

from qgis.core import QgsProject
from .topazeutils import TopazeUtils

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "field_data_dialog.ui")
)


class FieldDataDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(FieldDataDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        self.toolButton.pressed.connect(self.showDialog)

    def showDialog(self):

        fname, _filter = QtWidgets.QFileDialog.getOpenFileName(
            self,
            'Open terrain data file',
            QgsProject.instance().homePath() + "/terrain/",
            "All files (*.*);;Leica GSI (*.gsi);;Trimble ARE (*.are);;Trimble Job (*.job);;Text (*.txt);;Azimut (*.obs)",
        )

        if fname:
            field_data_type = TopazeUtils.get_field_data_type(fname)
            if field_data_type:
                self.label_format.setText(field_data_type)
            else:
                self.label_format.setText(self.tr("Unknown"))

            self.lineEdit.setText(fname)
