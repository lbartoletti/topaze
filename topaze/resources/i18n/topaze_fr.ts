<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>EditObsDialog</name>
    <message>
        <location filename="../../field_data_dialog.ui" line="14"/>
        <source>Points</source>
        <translation type="obsolete">Points</translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="42"/>
        <source>Field data</source>
        <translation type="obsolete">Données terrain</translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="66"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="107"/>
        <source>Field data file :</source>
        <translation type="obsolete">Fichier terrain :</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="14"/>
        <source>DICT</source>
        <translation type="obsolete">DICT</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="26"/>
        <source>Observations</source>
        <translation>Observations</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="49"/>
        <source>Edition</source>
        <translation>Edition</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="62"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="67"/>
        <source>st</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="72"/>
        <source>ref</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="77"/>
        <source>pnt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="82"/>
        <source>hp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="87"/>
        <source>cpt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="92"/>
        <source>cli</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="97"/>
        <source>xyz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="102"/>
        <source>ch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="107"/>
        <source>da</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="112"/>
        <source>cmd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="117"/>
        <source>infos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="122"/>
        <source>code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="145"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="150"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="155"/>
        <source>P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="189"/>
        <source>Sloped dis.</source>
        <translation>Dis. inc.</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="212"/>
        <source>Ver. Ang.</source>
        <translation>Ang. ver.</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="235"/>
        <source>Hor. Ang.</source>
        <translation>Ang. hor.</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="248"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="281"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="304"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="327"/>
        <source>Target Ht.</source>
        <translation>Ht. prisme</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="350"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="442"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="386"/>
        <source>Instr. Ht.</source>
        <translation>Ht. instr.</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="419"/>
        <source>v0</source>
        <translation>v0</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="483"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="496"/>
        <source>Change</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="509"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="522"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="535"/>
        <source>Print selection</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../../edit_obs_dialog.ui" line="548"/>
        <source>Cancel</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="75"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="124"/>
        <source>Report an issue</source>
        <translation>Rapporter un problème</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="146"/>
        <source>Version used to save settings:</source>
        <translation>Version utilisée pour enregistrer les paramètres :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="168"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="190"/>
        <source>Reset setttings to factory defaults</source>
        <translation>Réinitialiser les paramètres aux valeurs par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="209"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="218"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode debug</translation>
    </message>
</context>
<context>
    <name>PlgTranslator</name>
    <message>
        <location filename="../../toolbelt/translator.py" line="68"/>
        <source>Your selected locale ({}) is not available. Please consider to contribute with your own translation :). Contact the plugin maintener(s): {}</source>
        <translation>Votre choix de locale ({}) n&apos;est pas disponible. Contribuez avec votre propre traduction svp :). Contactez le(s) mainteneur(s) du plugin : {}</translation>
    </message>
</context>
<context>
    <name>TopazePlugin</name>
    <message>
        <location filename="../../plugin_main.py" line="125"/>
        <source>&amp;Topaze</source>
        <translation>&amp;Topaze</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="133"/>
        <source>Process field codification</source>
        <translation>Traiter la codification terrain</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="905"/>
        <source>Edit observations</source>
        <translation>Editer les observations</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="155"/>
        <source>Compute reference points</source>
        <translation>Calculer le canevas</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="166"/>
        <source>Update observations</source>
        <translation>Importer le carnet</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="199"/>
        <source>Compute points</source>
        <translation>Calculer les points de détail</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="210"/>
        <source>Purge observations</source>
        <translation>Effacer le carnet</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="221"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="230"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="101"/>
        <source>PTOPO table missing</source>
        <translation>Table des PTOPO absente</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="111"/>
        <source>Observations table missing</source>
        <translation>Table du carnet absente</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="412"/>
        <source>Unable to add points</source>
        <translation>Impossible d&apos;ajouter les points</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="668"/>
        <source>Unable to get field data</source>
        <translation>Impossible d&apos;obtenir les données du carnet</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="493"/>
        <source>Ptopo {ptopo.matricule} updated in array</source>
        <translation type="obsolete">Ptopo {ptopo.matricule} mis à jour dans le tableau</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="499"/>
        <source>Ptopo {ptopo.matricule} added in array</source>
        <translation type="obsolete">Ptopo {ptopo.matricule} ajouté dans le tableau</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="502"/>
        <source>Surveyed points computed</source>
        <translation type="obsolete">Points rayonnés calculés</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="491"/>
        <source> {len(self._ptopo_array)} points</source>
        <translation type="obsolete"> {len(self._ptopo_array)} points</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="621"/>
        <source>Unable to open {fullfilepath}</source>
        <translation type="obsolete">Impossible d&apos;ouvrir  {fullfilepath}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="603"/>
        <source>Unable to update observations with field data</source>
        <translation>Impossible de mettre à jour le carnet avec les données terrain</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="609"/>
        <source>Observations updated</source>
        <translation>Carnet mis à jour</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="599"/>
        <source> with field data from {fullfilepath}</source>
        <translation type="obsolete"> avec les données de {fullfilepath}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="696"/>
        <source>Unable to set field observations in table</source>
        <translation>Impossible de stocker le carnet dans la table</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="779"/>
        <source>Unable to erase observations</source>
        <translation>Impossible d&apos;effacer le carnet</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="835"/>
        <source>Unable to add observation</source>
        <translation>Impossible d&apos;ajouter l&apos;observation</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="883"/>
        <source>Unable to save field observations</source>
        <translation>Impossible d&apos;enregistrer les observations terrain</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="888"/>
        <source>Field observations data saved</source>
        <translation>Observations terrain enregistrées</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="79"/>
        <source>No translation file for {}</source>
        <translation>Pas de fichier de traduction pour {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="177"/>
        <source>New survey</source>
        <translation>Nouveau lever</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="538"/>
        <source>Unable to create new survey </source>
        <translation>Impossible de créer le nouveau lever </translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="544"/>
        <source>New survey created </source>
        <translation>Nouveau lever créé </translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="535"/>
        <source> in {to_path}</source>
        <translation type="obsolete"> dans {to_path}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="188"/>
        <source>Import points</source>
        <translation>Importer points</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="359"/>
        <source>Point({ptopo.x} {ptopo.y} {PtopoConst.NO_Z})</source>
        <translation type="obsolete">Point({ptopo.x} {ptopo.y} {PtopoConst.NO_Z})</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="634"/>
        <source>Unable to import points from {fullfilepath}</source>
        <translation type="obsolete">Impossible d&apos;importer points depuis le fichier {fullfilepath}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="630"/>
        <source>{len(new_points)} points imported from {fullfilepath}</source>
        <translation type="obsolete">{len(new_points)} points importés depuis le fichier {fullfilepath}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="502"/>
        <source> {len(self._ptopo_array)} points ({n_cre} created, {n_upd} updated)</source>
        <translation type="obsolete"> {len(self._ptopo_array)} points ({n_cre} créés, {n_upd} mis-à-jour)</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="639"/>
        <source>{nb} points imported from {fullfilepath}</source>
        <translation type="obsolete">{nb} points importés de {fullfilepath}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="788"/>
        <source>Observations erased</source>
        <translation>Carnet effacé</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="505"/>
        <source>{} surveyed point(s) computed</source>
        <translation>{} point(s) levé(s) calculé(s)</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="649"/>
        <source> ({} created, {} updated)</source>
        <translation> ({} créé(s), {} mis à jour)</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="544"/>
        <source> in {}</source>
        <translation> dans {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="631"/>
        <source>Unable to open {}</source>
        <translation>Impossible d&apos;ouvrir {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="609"/>
        <source> with field data from {}</source>
        <translation> avec les données terrain de {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="644"/>
        <source>Unable to import points from {}</source>
        <translation>Impossible d&apos;importer les points de {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="649"/>
        <source>{} point(s) imported from {}</source>
        <translation>{} point(s) importé(s) de {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="905"/>
        <source>Do you want to save or cancel and drop your changes ?</source>
        <translation>Voulez-vous enregistrer ou annuler et abandonner vos modifications ?</translation>
    </message>
</context>
<context>
    <name>Topaze_import_points</name>
    <message>
        <location filename="../../import_points_dialog.ui" line="14"/>
        <source>TOPAZE: import points</source>
        <translation>TOPAZE : importer des points</translation>
    </message>
    <message>
        <location filename="../../import_points_dialog.ui" line="42"/>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <location filename="../../import_points_dialog.ui" line="107"/>
        <source>Points file :</source>
        <translation>Fichier des points :</translation>
    </message>
</context>
<context>
    <name>Topaze_new_survey</name>
    <message>
        <location filename="../../new_survey_dialog.ui" line="14"/>
        <source>New survey</source>
        <translation>Nouveau lever</translation>
    </message>
    <message>
        <location filename="../../new_survey_dialog.ui" line="42"/>
        <source>Choose directory</source>
        <translation>Choisir le dossier</translation>
    </message>
    <message>
        <location filename="../../new_survey_dialog.ui" line="107"/>
        <source>New survey folder :</source>
        <translation>Dossier lever :</translation>
    </message>
</context>
<context>
    <name>Topaze_update_observations</name>
    <message>
        <location filename="../../field_data_dialog.ui" line="14"/>
        <source>DICT</source>
        <translation type="obsolete">DICT</translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="42"/>
        <source>Field data</source>
        <translation>Données terrain</translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="107"/>
        <source>Field data file :</source>
        <translation>Fichier terrain :</translation>
    </message>
    <message>
        <location filename="../../field_data_dialog.ui" line="14"/>
        <source>TOPAZE: update observations</source>
        <translation>TOPAZE : importer le carnet</translation>
    </message>
</context>
</TS>
