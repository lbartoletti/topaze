class Observation:

    __slots__ = (
        'type','matricule', 'hi', 'v0', 'hp', 'hpp', 'codes','x', 'y', 'z',
        'ah', 'av', 'dz', 'di', 'dh'
        )
    def __init__(self, typeobs, obsdict):
        """ Constructor
        :param matricule: field observed ptopo name
        """
        self.type = typeobs
        self.matricule = None
        self.hi = self.v0 = self.hp = self.hpp = self.codes = None
        self.x = self.y = self.z = None
        self.ah = self.av = self.dz = self.di = self.dh = None
        if 'st' in obsdict.keys():
            self.matricule = obsdict['st']
        elif 'ref' in obsdict.keys():
            self.matricule = obsdict['ref']
        elif 'pnt' in obsdict.keys():
            self.matricule = obsdict['pnt']
        if 'hi' in obsdict.keys():
            self.hi = float(obsdict['hi'])
        if 'v0' in obsdict.keys():
            self.v0 = float(obsdict['v0'])
        if 'hp' in obsdict.keys():
            self.hp = float(obsdict['hp'])
        if 'hpp' in obsdict.keys():
            self.hpp = float(obsdict['hpp'])
        if 'x' in obsdict.keys():
            self.x = float(obsdict['x'])
        if 'y' in obsdict.keys():
            self.y = float(obsdict['y'])
        if 'z' in obsdict.keys():
            self.z = float(obsdict['z'])
        if 'ah' in obsdict.keys():
            self.ah = float(obsdict['ah'])
        if 'av' in obsdict.keys():
            self.av = float(obsdict['av'])
        if 'dz' in obsdict.keys():
            self.dz = float(obsdict['dz'])
        if 'di' in obsdict.keys():
            self.di = float(obsdict['di'])
        if 'dh' in obsdict.keys():
            self.dh = float(obsdict['dh'])
        if 'code' in obsdict.keys():
            self.codes = obsdict['code']