#! /usr/bin/env python
# -*- coding: utf-8 -*-
# filename: tops_azimut.py
# Copyright 2022 Jean-Marie Arsac <jmarsac@azimut.fr>
#
# This file is part of Total Open Station.
#
# Total Open Station is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Total Open Station is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Total Open Station.  If not, see
# <http://www.gnu.org/licenses/>.

import io

from . import Builder



class OutputFormat(Builder):

    """
    Exports points data in azimut obs format.

    ``data`` should be an iterable containing Feature objects.
    """

    def __init__(self, data):
        self.data = data
        self.output = io.StringIO()
        fieldnames = ['type', 'pnt', 'x', 'y', 'z', 'ah', 'av', 'di', 'dh',
                      'hp', 'hi', 'circle', 'st', 'ref', 'code', 'infos']

    def process(self):

        lines = []
        for feature in self.data:
            row = {
                'type': feature.desc,
            }

            props = {'st_name': 'st', 'point_name': 'pnt', 'ih': 'hi',
                'angle': 'ah', 'z_angle': 'av', 'distance': 'di', 'th': 'hp',
                'ref_name': 'ref', 'attrib': 'infos', 'code': 'code'}
            # a few cases with simple yes/no logic
            for prop in props.keys():
                row[props[prop]] = feature.properties.get(prop, '?')  # interrogation string as default value

            # azimuth/angle
            row['ah'] = feature.properties.get('azimuth',
                feature.properties.get('angle', ''))

            # vertical angle
            row['av'] = feature.properties.get('z_angle', '')

            # not all input formats include distance
            dist_type = feature.properties.get('dist_type', '?' )
            if dist_type == 's':
                row['di'] = feature.properties.get('dist', '')
            elif dist_type == 'h':
                row['dh'] = feature.properties.get('dist', '')

            # not all input formats include station name
            row['st'] = feature.properties.get('st_name', '')

            row['hi'] = feature.properties.get('ih', '')

            row['hp'] = feature.properties.get('th', '')

            line = self.to_obs(feature.geometry, row)
            if line:
                lines.append(line)

        output = "\n".join(lines)

        return output

    def to_obs(self, pt, row):
        obs = ''
        if row['type'] in ["PT", "PO", "ST", "P", "S", "R"]:
            if  row['type'] == "ST" and row['st']:
                obs = f"st={row['st']}"
                if row['hi']:
                    obs = obs + f" hi={row['hi']}"
                if pt and pt.x >= 0.:
                    obs = (obs + "\n" +
                        f"st={row['st']} x={pt.x} y={pt.y} z={pt.z}")
            elif row['ref'] and row['ref'] != '?' and row['ah'] and row['av'] and row['di']:
                obs = f"ref={row['ref']} ah={row['ah']} av={row['av']} di={row['di']}"
            elif row['ref'] and row['ref'] != '?' and row['ah'] and row['av']:
                obs = f"ref={row['ref']} ah={row['ah']} av={row['av']}"
            elif row['ref'] and row['ref'] != '?' and row['ah']:
                obs = f"ref={row['ref']} ah={row['ah']}"
            elif row['ref'] and row['ref'] != '?' and pt and pt.x >= 0.:
                obs = f"ref={row['ref']} x={pt.x} y={pt.y} z={pt.z}"
            elif row['pnt'] and row['ah'] and row['av'] and row['di']:
                obs = f"pnt={row['pnt']} ah={row['ah']} av={row['av']} di={row['di']}"
            elif row['pnt'] and row['ah'] and row['av']:
                obs = f"pnt={row['pnt']} ah={row['ah']} av={row['av']}"
            elif row['pnt'] and pt and pt.x >= 0.:
                obs = f"pnt={row['pnt']} x={pt.x} y={pt.y} z={pt.z}"
            if row['hp'] and  float(row['hp']) > 0.:
                obs = f"hp={row['hp']}\n" + obs
            if row['code'] and row['code'] != ' ' and row['code'] != '?':
                if obs:
                    obs = obs + f"\ncode={row['code']}"
                else:
                    obs = f"code={row['code']}"
        elif row['type'] == "CO":
            return row['infos'].replace(' ', ';')
        elif 'infos' in row.keys():
            return f"infos={row['infos']}"
        return obs