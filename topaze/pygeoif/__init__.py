# -*- coding: utf-8 -*-
#
#   Copyright (C) 2012 - 2022 Christian Ledermann
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.

#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.

#   You should have received a copy of the GNU Lesser General Public License
#   along with this library; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#
"""PyGeoIf provides a GeoJSON-like protocol for geo-spatial (GIS) vector data."""
from topaze.pygeoif.factories import from_wkt
from topaze.pygeoif.factories import mapping
from topaze.pygeoif.factories import orient
from topaze.pygeoif.factories import shape
from topaze.pygeoif.feature import Feature
from topaze.pygeoif.feature import FeatureCollection
from topaze.pygeoif.geometry import GeometryCollection
from topaze.pygeoif.geometry import LinearRing
from topaze.pygeoif.geometry import LineString
from topaze.pygeoif.geometry import MultiLineString
from topaze.pygeoif.geometry import MultiPoint
from topaze.pygeoif.geometry import MultiPolygon
from topaze.pygeoif.geometry import Point
from topaze.pygeoif.geometry import Polygon

__all__ = [
    "Feature",
    "FeatureCollection",
    "GeometryCollection",
    "LineString",
    "LinearRing",
    "MultiLineString",
    "MultiPoint",
    "MultiPolygon",
    "Point",
    "Polygon",
    "from_wkt",
    "mapping",
    "orient",
    "shape",
]
