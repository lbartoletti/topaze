# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ImportPointsDialog
                                 A QGIS plugin
 Topaze
                             -------------------
        begin                : 2022-09-29
        git sha              : $Format:%H$
        copyright            : (C) 2022 by Jean-Marie ARSAC
        email                : jmarsac@arsac.wf
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt5 import QtCore, QtWidgets, uic

from qgis.core import QgsProject

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "import_points_dialog.ui")
)


class ImportPointsDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(ImportPointsDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        self.toolButton.pressed.connect(self.showDialog)

    def showDialog(self):

        fname, _filter = QtWidgets.QFileDialog.getOpenFileName(
            self,
            'Open points file',
            QgsProject.instance().homePath() + "/terrain/",
            "Points (*.jsi, *.txt);;All files (*)",
        )

        if fname:
            f = open(fname, mode="r")

            with f:
                self.lineEdit.setText(fname)
                f.close()
